<?php


namespace app\redisdata;


use testtask\queue\Data;

class UserFriends extends Data
{
    protected string $dataName = 'friends';
}