<?php


namespace app\queues;


use app\repositories\RelationsRepository;
use testtask\queue\Queue;

/**
 * Class RemoveRelationQueue
 * @package app\queues
 */
class RemoveRelationQueue extends Queue
{
    /** Get relation type */
    protected function type(): int
    {
        return RelationsRepository::TYPE_REMOVE_RELATION;
    }
}