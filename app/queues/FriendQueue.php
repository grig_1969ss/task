<?php


namespace app\queues;


use app\repositories\RelationsRepository;
use testtask\queue\Queue;

/**
 * Class FriendQueue
 * @package app\queues
 */
class FriendQueue extends Queue
{
    /** Get relation type */
    protected function type(): int
    {
        return RelationsRepository::TYPE_FRIEND;
    }
}