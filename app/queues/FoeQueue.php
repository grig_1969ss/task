<?php


namespace app\queues;


use app\repositories\RelationsRepository;
use testtask\queue\Queue;

/**
 * Class FoeQueue
 * @package app\queues
 */
class FoeQueue extends Queue
{
    /** Get relation type */
    protected function type(): int
    {
        return RelationsRepository::TYPE_FOE;
    }


}