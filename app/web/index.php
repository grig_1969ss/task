<?php

require "../../vendor/autoload.php";

use app\repositories\UserRepository;
use app\models\User;
use app\models\UserRelations as Relation;

//delete all data from users
UserRepository::getInstance()->deleteAll();

//Add some users
UserRepository::getInstance()->insertOne('Tom');//id 1
UserRepository::getInstance()->insertOne('John');//id 2
UserRepository::getInstance()->insertOne('Paul');//id 3
UserRepository::getInstance()->insertOne('Kevin');//id 4
UserRepository::getInstance()->insertOne('Michael');//id 5
UserRepository::getInstance()->insertOne('Jerry');//id 6
UserRepository::getInstance()->insertOne('Vahe');//id 7
UserRepository::getInstance()->insertOne('Dean');//id 8
UserRepository::getInstance()->insertOne('George');//id 9
UserRepository::getInstance()->insertOne('Cat');//id 10

//Get Users Models By Id | STEP 1
$tom = new User(1);
$john = new User(2);
$paul = new User(3);
$kevin = new User(4);
$michael = new User(5);
$jerry = new User(6);
$vahe = new User(7);
$dean = new User(8);
$george = new User(9);
$cat = new User(10);

//Get Repository for each user | STEP 2
$tomRelation = new Relation($tom);
$johnRelation = new Relation($john);
$paulRelation = new Relation($paul);
$kevinRelation = new Relation($kevin);
$michaelRelation = new Relation($michael);
$jerryRelation = new Relation($jerry);
$vaheRelation = new Relation($vahe);
$deanRelation = new Relation($dean);
$georgeRelation = new Relation($george);
$catRelation = new Relation($cat);

////Add relations | STEP 3
//$tomRelation->addFoe($jerry);// TOM AND JERRY ARE FOES
//$johnRelation->addFriend($paul);//JOHN AND TOM ARE FRIENDS
//$johnRelation->addFriend($kevin);
//$johnRelation->addFriend($vahe);
//$vaheRelation->addFriend($dean);
//$deanRelation->addFriend($george);
//$georgeRelation->addFoe($paul);
//


//remove relations

//if($johnRelation->isFriend($kevin) || $johnRelation->isFoe($kevin)) {
//    $johnRelation->removeRelation($kevin);
//}

//Show Friends OR Foes

//print_r($paulRelation->getAllFriends(9));
//print_r($paulRelation->getAllFoes());
//print_r($tomRelation->getAllFriends(1));//2 Level Of Friends
//print_r($tomRelation->getConflictUsers());//2 Level Of Friends