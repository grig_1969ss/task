<?php


namespace app\repositories;


use app\facades\DB;
use testtask\traits\TSingleton;

/**
 * Class UserRepository
 * @package app\repositories
 */
class UserRepository
{
    use TSingleton;

    protected string $tableName = 'users';

    /**
     * @param string $name
     * @return bool
     */
    public function insertOne(string $name): bool
    {
        $query = DB::get()->query("INSERT INTO $this->tableName (name) VALUES (:name)", [
            'name' => $name
        ]);

        return $query ? true : false;
    }
    
    /**
     * Insert array of key => value pairs
     * [
     *   'user1' => 'John',
     *   'user2' => 'Grigor'
     * ]
     *
     * @param array $usersList
     * @return bool
     */
    public function insertMany(array $usersList): bool
    {
        $usersListBind = array_keys($usersList);

        array_walk($usersListBind, array($this, 'bindParamName'));

        $bindString = implode(',', $usersListBind);
        $query = DB::get()->query("INSERT INTO $this->tableName (name) VALUES $bindString",
            $usersList);

        return $query ? true : false;
    }

    /**
     * Update one element in table
     *
     * @param int $id
     * @param string $name
     * @return bool
     */
    public function updateName(int $id, string $name): bool
    {
        $query = DB::get()->query("UPDATE $this->tableName SET name = :name WHERE id = :id", [
                'id' => $id,
                'name' => $name
            ]);

        return $query ? true : false;
    }


    /**
     * @return |null
     */
    public function findAll()
    {
        return DB::get()->query("SELECT * FROM $this->tableName");
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findOne(int $id)
    {
        return DB::get()->bind('id', $id)
            ->row("SELECT * FROM $this->tableName WHERE id = :id");
    }


    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $query = DB::get()->query("DELETE FROM $this->tableName WHERE id = :id", [
            'id' => $id
        ]);

        return $query ? true : false;
    }

    /**
     * @return bool
     */
    public function deleteAll(): bool
    {
        $query = DB::get()->query("TRUNCATE TABLE $this->tableName");

        return $query ? true : false;
    }

    /**
     * Method for array_walk function
     *
     * @param $value
     * @param $key
     */
    private function bindParamName(&$value, $key)
    {
        $value = '(:' . $value . ')';
    }
}