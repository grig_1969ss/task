<?php


namespace app\repositories;


use app\facades\DB;
use testtask\traits\TSingleton;

class RelationsRepository
{
    use TSingleton;

    const TYPE_FRIEND = 1;
    const TYPE_FOE = 2;
    const TYPE_REMOVE_RELATION = 3;

    protected string $tableName = 'users_relations';


    public function getDirectRelations()
    {
        return DB::get()
            ->query("SELECT users.id AS userId,ur.user1,ur.user2,ur.type
                    FROM users
                            LEFT JOIN users_relations ur ON ur.user1 = users.id OR ur.user2 = users.id
                            
                            WHERE ur.user1 IS NOT NULL OR ur.user2 IS NOT NULL ");
    }

    /**
     * @param string $name
     * @return bool
     */
    public function insertOne(int $user1, int $user2, int $type = self::TYPE_FRIEND): bool
    {
        if($user1 > $user2) {
            $min = $user2;
            $user2 = $user1;
            $user1 = $min;
        }

        $query = DB::get()
            ->query("INSERT INTO $this->tableName (user1,user2,type) VALUES (:user1,:user2,:type)", [
                'user1' => $user1,
                'user2' => $user2,
                'type' => $type,
            ]);

        return $query ? true : false;
    }

    /**
     * @param int $user1
     * @param int $user2
     * @param int $type
     * @return bool
     */
    public function updateRelation(int $user1, int $user2, int $type = self::TYPE_FRIEND): bool
    {
        $query = DB::get()->query("UPDATE $this->tableName SET type = :type WHERE (user1 = :user1 
            AND user2 = :user2) OR (user1 = :user2 
            AND user2 = :user1)", [
            'user1' => $user1,
            'user2' => $user2,
            'type' => $type,
        ]);

        return $query ? true : false;
    }

    /**
     * @param int $user1
     * @param int $user2
     * @param int $type
     * @return bool
     */
    public function deleteOne(int $user1, int $user2): bool
    {
        $query = DB::get()->query("DELETE FROM $this->tableName WHERE (user1 = :user1 
            AND user2 = :user2) OR (user1 = :user2 
            AND user2 = :user1)", [
            'user1' => $user1,
            'user2' => $user2
        ]);

        return $query ? true : false;
    }

    /**
     * @return |null
     */
    public function findAll()
    {
        return DB::get()->query("SELECT * FROM $this->tableName");
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function findOne(int $id)
    {
        return DB::get()->bind('id', $id)
            ->row("SELECT * FROM $this->tableName WHERE id = :id");
    }

    /**
     * @param array $params
     * @return mixed
     */
    public function findRelation(array $params)
    {
        return DB::get()->bindMore($params)
            ->row("SELECT * FROM $this->tableName WHERE  (user1 = :user1 
            AND user2 = :user2) OR (user1 = :user2 
            AND user2 = :user1)");
    }

    /**
     * @param int $id
     * @return bool
     */
    public function delete(int $id): bool
    {
        $query = DB::get()->query("DELETE FROM $this->tableName WHERE id = :id", [
            'id' => $id
        ]);

        return $query ? true : false;
    }

    /**
     * @return bool
     */
    public function deleteAll(): bool
    {
        $query = DB::get()->query("TRUNCATE TABLE $this->tableName");

        return $query ? true : false;
    }

    /**
     * Method for array_walk function
     *
     * @param $value
     * @param $key
     */
    private function bindParamName(&$value, $key)
    {
        $value = '(:' . $value . ')';
    }
}