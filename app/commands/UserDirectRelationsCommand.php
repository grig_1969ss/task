<?php


namespace app\commands;

use app\redisdata\UserFoes;
use app\redisdata\UserFriends;
use app\repositories\RelationsRepository;
use app\repositories\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class UserDirectRelationsCommand
 * @package app\commands
 */
class UserDirectRelationsCommand extends Command
{

    protected static $defaultName = 'user-direct';

    private const MAX_COUNT = 1000;

    private ?UserRepository $userRepository;
    private ?RelationsRepository $relationsRepository;

    protected function configure()
    {
        $this->userRepository = UserRepository::getInstance();
        $this->relationsRepository = RelationsRepository::getInstance();
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $usersRelations = $this->relationsRepository->getDirectRelations();
        $users = $this->userRepository->findAll();

        $friendList = [];
        $foeList = [];

        foreach ($usersRelations as $userRelation) {
            if ($userRelation['type'] == RelationsRepository::TYPE_FRIEND) {
                $friendList[$userRelation['userId']] = $this->addUserFriend(
                    $friendList[$userRelation['userId']] ?? [],
                    $userRelation);
            } elseif ($userRelation['type'] == RelationsRepository::TYPE_FOE) {
                $foeList[$userRelation['userId']] = $this->addUserFoe(
                    $foeList[$userRelation['userId']] ?? [],
                    $userRelation);
            }
        }

        foreach ($users as $user) {
            if(!isset($friendList[$user['id']])) {
                (new UserFriends([
                    'id' => $user['id']
                ]))->remove();
            } else {
                new UserFriends([
                    'id' => $user['id'],
                    'data' => $friendList[$user['id']]
                ]);
            }

            if(!isset($foeList[$user['id']])) {
                (new UserFoes([
                    'id' => $user['id']
                ]))->remove();
            } else {
                new UserFoes([
                    'id' => $user['id'],
                    'data' => $foeList[$user['id']]
                ]);
            }
        }

        foreach ($friendList as $userId => $data) {

        }

        foreach ($foeList as $userId => $data) {

        }

        return Command::SUCCESS;
    }

    /**
     * @param array $relation
     * @return int
     */
    private function getUserByRelation(array $relation): int
    {
        return $relation['user1'] == $relation['userId'] ? $relation['user2'] : $relation['user1'];
    }

    /**
     * @param array $friendList
     * @param array $relation
     * @return array
     */
    private function addUserFriend(array $friendList, array $relation)
    {
        if (!$friendList) {
            $friendList = [$this->getUserByRelation($relation)];
        } else {
            $friendList[] = $this->getUserByRelation($relation);
        }

        return $friendList;
    }

    /**
     * @param array $foeList
     * @param array $relation
     * @return array
     */
    private function addUserFoe(array $foeList, array $relation)
    {
        if (!$foeList) {
            $foeList = [$this->getUserByRelation($relation)];
        } else {
            $foeList[] = $this->getUserByRelation($relation);
        }

        return $foeList;
    }

}