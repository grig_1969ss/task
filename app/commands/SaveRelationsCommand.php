<?php


namespace app\commands;


use app\queues\FoeQueue;
use app\queues\FriendQueue;
use app\queues\RemoveRelationQueue;
use app\redisdata\UserFoes;
use app\redisdata\UserFriends;
use app\repositories\RelationsRepository;
use app\repositories\UserRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use testtask\queue\Queue;

class SaveRelationsCommand extends Command
{
    protected static $defaultName = 'save-relations';

    private const MAX_COUNT = 1000;

    private ?UserRepository $userRepository;
    private ?RelationsRepository $relationsRepository;

    protected function configure()
    {
        $this->userRepository = UserRepository::getInstance();
        $this->relationsRepository = RelationsRepository::getInstance();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $queueList = (new FriendQueue())->getData();
        $count = count($queueList);
        if ($count >= self::MAX_COUNT) {
            $queueList = array_slice($queueList, 0, self::MAX_COUNT);
        }

        foreach ($queueList as $item) {
            if (!$existingRelation = $this->checkRelationExists([
                    'user1' => $item['user1'],
                    'user2' => $item['user2']]
            )) {
                if ($item['type'] !== RelationsRepository::TYPE_REMOVE_RELATION) {
                    $this->relationsRepository
                        ->insertOne($item['user1'], $item['user2'], $item['type']);
                } else {
                    $this->relationsRepository
                        ->deleteOne($item['user1'], $item['user2']);
                }
                $this->removeFromQueue($item);
            } else {
                if ($item['type'] != RelationsRepository::TYPE_REMOVE_RELATION) {
                    if ($item['type'] != $existingRelation['type']) {
                        $this->relationsRepository
                            ->updateRelation($existingRelation['user1'], $existingRelation['user2'], $item['type']);
                        $this->removeFromQueue($existingRelation ?? []);
                    } else {
                        $this->removeFromQueue($item);
                    }
                } else {
                    $this->relationsRepository
                        ->deleteOne($existingRelation['user1'], $existingRelation['user2']);
                    unset($existingRelation['id']);
                    $existingRelation['type'] = RelationsRepository::TYPE_REMOVE_RELATION;
                    $this->removeFromQueue($existingRelation ?? []);
                }
            }
        }

        return Command::SUCCESS;
    }

    /**
     * @param array $item
     */
    private function removeFromQueue(array $item)
    {
        $queue = $this->getQueueByType($item['type']);

        $queue->removeItem($item);
    }

    /**
     * @param int $type
     * @return ?Queue
     */
    private function getQueueByType(int $type): ?Queue
    {
        return [
            RelationsRepository::TYPE_FRIEND => new FriendQueue(),
            RelationsRepository::TYPE_FOE => new FoeQueue(),
            RelationsRepository::TYPE_REMOVE_RELATION => new RemoveRelationQueue()
        ][$type];
    }

    /**
     * @param array $relation
     * @return bool
     */
    private function checkRelationExists(array $relation)
    {
        return $this->relationsRepository->findRelation($relation);
    }
}