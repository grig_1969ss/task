<?php

namespace app\facades;

use testtask\traits\TSingleton;
use testtask\db\DB as DBObject;

class DB
{
    public static function get(): DBObject
    {
        return DBObject::getInstance()->setConnection(
            '127.0.0.1',
            'test',
            'root',
            ''
        );
    }
}