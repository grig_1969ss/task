<?php


namespace app\services;


use app\queues\FoeQueue;
use app\queues\FriendQueue;
use app\queues\RemoveRelationQueue;
use app\redisdata\UserFoes;
use app\redisdata\UserFriends;
use testtask\traits\TSingleton;

class UserService
{
    use TSingleton;
    
    //Friends Or Foes List Current Level
    private int $currentLevel = 0;
    
    //Friends Or Foes List Max Level
    private int $maxLevel = 0;
    
    //Friends List
    private array $friendList = [];
    
    //Foes List
    private array $foeList = [];

    /**
     * @param int $user1
     * @param int $user2
     * @return bool
     */
    public function addFriend(int $user1, int $user2): bool
    {
        $queue = new FriendQueue([
            'user1' => $user1,
            'user2' => $user2
        ]);

        return $queue->saved();
    }

    /**
     * @param int $user1
     * @param int $user2
     * @return bool
     */
    public function addFoe(int $user1, int $user2): bool
    {
        $queue = new FoeQueue([
            'user1' => $user1,
            'user2' => $user2
        ]);

        return $queue->saved();
    }

    /**
     * @param int $user1
     * @param int $user2
     * @return bool
     */
    public function removeRelation(int $user1, int $user2): bool
    {
        $queue = new RemoveRelationQueue([
            'user1' => $user1,
            'user2' => $user2
        ]);

        return $queue->saved();
    }

    /**
     * @param int $user1
     * @param int $user2
     * @return bool
     */
    public function isFriend(int $user1, int $user2): bool
    {
        return in_array($user2, (new UserFriends(['id' => $user1]))->getData());
    }

    /**
     * @param int $user1
     * @param int $user2
     * @return bool
     */
    public function isFoe(int $user1, int $user2): bool
    {
        return in_array($user2, (new UserFoes(['id' => $user1]))->getData());
    }

    /**
     * @param int $userId
     * @param int $level
     * @return array
     */
    public function getAllFriends(int $userId, int $level = 0): array
    {
        $this->currentLevel = 0;
        $this->maxLevel = $level;

        $friendsList = $this->getFriends($userId);

        if (($key = array_search($userId, $friendsList)) !== false) {
            unset($friendsList[$key]);
        }

        return $friendsList;
    }

    /**
     * @param int $userId
     * @param int $level
     * @return array
     */
    public function getAllFoes(int $userId, int $level = 0): array
    {
        $this->currentLevel = 0;
        $this->maxLevel = $level;

        $foesList = $this->getFoes($userId);

        if (($key = array_search($userId, $foesList)) !== false) {
            unset($foesList[$key]);
        }

        return $foesList;
    }

    /**
     * @param int $userId
     * @param int $level
     * @return array
     */
    public function getConflictedUsers(int $userId, int $level = 0): array
    {
        $foesList = $this->getAllFoes($userId,$level);
        $friendsList = $this->getAllFoes($userId,$level);

        return array_intersect($foesList, $friendsList);
    }

    /**
     * Recursive Method
     * 
     * @param int $userId
     * @return array
     */
    private function getFriends(int $userId): array
    {
        if($this->maxLevel && $this->currentLevel++ >= $this->maxLevel) {
            return $this->friendList;
        }

        $friendsList = array_unique((new UserFriends(['id' => $userId]))->getData());

        if(!$friendsList) {
            return $this->friendList;
        }

        foreach ($friendsList as $friendId) {
            if(!in_array($friendId, $this->friendList)) {
                $this->friendList[] = $friendId;
                $this->getFriends($friendId);
            }
        }

        return $this->friendList;
    }

    /**
     * Recursive method
     * 
     * @param int $userId
     * @return array
     */
    private function getFoes(int $userId): array
    {
        if($this->maxLevel && $this->currentLevel++ >= $this->maxLevel) {
            return $this->foeList;
        }

        $foeList = (new UserFoes(['id' => $userId]))->getData();

        if(!$foeList) {
            return $this->foeList;
        }

        foreach ($foeList as $foeId) {
            if(!in_array($foeId, $this->foeList)) {
                $this->foeList[] = $foeId;
                $this->getFoes($foeId);
            }
        }

        return $this->foeList;
    }
}