<?php

namespace app\models;

use app\services\UserService;
use app\interfaces\IUser;
use app\interfaces\IUserRelations;

class UserRelations implements IUserRelations
{
    private ?IUser $user;

    /**
     * @inheritDoc
     */
    public function __construct(IUser $user)
    {
        $this->user = $user;
    }

    /**
     * @inheritDoc
     */
    public function addFriend(IUser $user): bool
    {
        return UserService::getInstance()->addFriend($this->user->getId(), $user->getId());
    }

    /**
     * @inheritDoc
     */
    public function addFoe(IUser $user): bool
    {
        return UserService::getInstance()->addFoe($this->user->getId(), $user->getId());
    }

    /**
     * @inheritDoc
     */
    public function removeRelation(IUser $user): bool
    {
        return UserService::getInstance()->removeRelation($this->user->getId(), $user->getId());
    }

    /**
     * @inheritDoc
     */
    public function isFriend(IUser $user, int $maxScanDepth = 0): bool
    {
        return UserService::getInstance()->isFriend($this->user->getId(), $user->getId());
    }

    /**
     * @inheritDoc
     */
    public function isFoe(IUser $user, int $maxScanDepth = 0): bool
    {
        return UserService::getInstance()->isFoe($this->user->getId(), $user->getId());
    }

    /**
     * @inheritDoc
     */
    public function getAllFriends(int $maxScanDepth = 0): array
    {
        echo $this->user->getId();
       return UserService::getInstance()->getAllFriends($this->user->getId(), $maxScanDepth);
    }

    /**
     * @inheritDoc
     */
    public function getAllFoes(int $maxScanDepth = 0): array
    {
       return UserService::getInstance()->getAllFoes($this->user->getId(), $maxScanDepth);
    }

    /**
     * @inheritDoc
     */
    public function getConflictUsers(int $maxScanDepth = 0): array
    {
        return UserService::getInstance()->getConflictedUsers($this->user->getId(), $maxScanDepth);
    }
}