<?php

namespace app\models;

//require_once "../interfaces/IUser.php";


use app\interfaces\IUser;
use app\repositories\UserRepository;

class User implements IUser
{
    private ?int $id;
    private ?string $name = null;

    /**
     * @inheritDoc
     */
    public function __construct(int $id)
    {
        $user = UserRepository::getInstance()->findOne($id);

        if($user) {
            $this->id = $user['id'];
            $this->name = $user['name'];
        } else {
            die("User $id not found");
        }

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

}