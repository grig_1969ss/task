<?php

require_once __DIR__ . '/vendor/autoload.php';

use Symfony\Component\Console\Application;

$app = new Application('Name', 'as');
$app->add(new \app\commands\SaveRelationsCommand());
//$app->add(new \app\commands\UserCommand());
$app->add(new \app\commands\UserDirectRelationsCommand());
$app->run();
