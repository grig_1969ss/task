<?php

namespace testtask\db;

use \PDO;
use testtask\traits\TSingleton;

/**
 * Class DB
 * @package testtask\db
 */
class DB
{
    use TSingleton;

    // @var, MySQL Hostname
    private $hostname = 'localhost';

    // @var, MySQL Database
    private $database;

    // @var, MySQL Username
    private $username;

    // @var, MySQL Password
    private $password;

    // @object, The PDO object
    private $pdo;

    // @object, PDO statement object
    private $sQuery;

    // @array,  The database settings
    private $settings;

    // @bool ,  Connected to the database
    private $bConnected = false;

    // @array, The parameters of the SQL query
    private $parameters;

    /**
     * @param $hostname
     * @param $database
     * @param $username
     * @param $password
     * @return $this
     */
    public function setConnection($hostname, $database, $username, $password): self
    {
        $this->Connect($hostname, $database, $username, $password);
        $this->parameters = array();

        return $this;
    }

    /**
     * @param $hostname
     * @param $database
     * @param $username
     * @param $password
     */
    private function Connect($hostname, $database, $username, $password)
    {
        global $settings;
        $dsn = 'mysql:dbname=' . $database . ';host=' . $hostname;
        try {
            $this->pdo = new PDO($dsn, $username, $password, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, true);
            $this->bConnected = true;
        } catch (PDOException $e) {
            die('Something went wrong');
        }
    }

    public function CloseConnection()
    {
        $this->pdo = null;
    }

    /**
     * @param $query
     * @param string $parameters
     */
    private function init($query, $parameters = "")
    {
        # Connect to database
        if (!$this->bConnected) {
            $this->Connect();
        }
        try {
            $this->sQuery = $this->pdo->prepare($query);

            $this->bindMore($parameters);

            // Bind params
            if (!empty($this->parameters)) {
                foreach ($this->parameters as $param) {
                    $parameters = explode("\x7F", $param);
                    $this->sQuery->bindParam($parameters[0], $parameters[1]);
                }
            }

            $this->success = $this->sQuery->execute();
        } catch (PDOException $e) {
            die($e->getMessage());
        }

        $this->parameters = array();
    }

    /**
     * @param $param
     * @param $value
     * @return $this
     */
    public function bind($param, $value): self
    {
        $this->parameters[sizeof($this->parameters)] = ":" . $param . "\x7F" . utf8_encode($value);
        
        return $this;
    }

    /**
     * @param $params
     * @return $this
     */
    public function bindMore($params): self 
    {
        if (empty($this->parameters) && is_array($params)) {
            $columns = array_keys($params);
            foreach ($columns as $i => &$column) {
                $this->bind($column, $params[$column]);
            }
        }
        
        return $this;
    }

    /**
     * @param $query
     * @param null $params
     * @param int $fetchmode
     * @return |null
     */
    public function query($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $query = trim($query);

        $this->init($query, $params);

        $rawStatement = explode(" ", $query);

        $statement = strtolower($rawStatement[0]);

        if ($statement === 'select' || $statement === 'show') {
            return $this->sQuery->fetchAll($fetchmode);
        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
            return $this->sQuery->rowCount();
        } else {
            return NULL;
        }
    }

    /**
     * @return mixed
     */
    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * @param $query
     * @param null $params
     * @return array|null
     */
    public function column($query, $params = null)
    {
        $this->init($query, $params);
        $Columns = $this->sQuery->fetchAll(PDO::FETCH_NUM);

        $column = null;

        foreach ($Columns as $cells) {
            $column[] = $cells[0];
        }

        return $column;

    }

    /**
     * @param $query
     * @param null $params
     * @param int $fetchmode
     * @return mixed
     */
    public function row($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $this->init($query, $params);
        return $this->sQuery->fetch($fetchmode);
    }

    /**
     * @param $query
     * @param null $params
     * @return mixed
     */
    public function single($query, $params = null)
    {
        $this->init($query, $params);
        return $this->sQuery->fetchColumn();
    }

}

