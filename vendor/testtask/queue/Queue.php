<?php


namespace testtask\queue;

use Predis\Client as Predis;
use testtask\traits\TSingleton;

/**
 * Class Queue
 * @package app\vendor\testtask\queue
 */
abstract class Queue
{
    protected ?Predis $client = null;

    private bool $saved = false;

    /**
     * Queue constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->client = Client::getInstance()->redis();

        if($params) {
            $this->client->set(
                $this->getQueueFullName(),
                $this->setData($params['user1'], $params['user2'])
            );
        }

        return $this;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return json_decode($this->client->get($this->getQueueFullName()) ?? '', true) ?? [];
    }

    /**
     * @return bool
     */
    public function saved(): bool
    {
        return $this->saved;
    }

    /**
     * @param int $user1
     * @param int $user2
     * @return string
     */
    protected function setData(int $user1, int $user2): string
    {
        $data = $this->getData();

        $newData = [
            'user1' => $user1,
            'user2' => $user2,
            'type' => $this->type()
        ];

        if($this->isValid($newData)) {
            $data[] = $newData;
            $this->saved = true;
        }

        return json_encode($data);
    }

    public function removeItem(array $item)
    {
        $oldData = $this->getData();

        $keysList = [];
        foreach ($oldData as $key => $value) {
            if($value == $item || $value == $this->reverseItem($item)) {
                unset($oldData[$key]);
            } else {
                echo 'ssss';
            }
        }

        $this->updateData($oldData);
    }

    protected function reverseItem(array $item): array
    {
        return [
          'user1' => $item['user2'],
          'user2' => $item['user1'],
          'type' => $item['type'],
        ];
    }

    /**
     * @param array $data
     */
    protected function updateData(array $data)
    {
        $this->client->set(
            $this->getQueueFullName(),
            json_encode($data)
        );
    }


    /**
     * @param array $data
     * @return bool
     */
    protected function isValid(array $data): bool
    {
        $oldData = $this->getData();

        $reverseData = [
            'user1' => $data['user2'],
            'user2' => $data['user1'],
            'type' => $data['type']
        ];

        return !in_array($data, $oldData) && 
            !in_array($reverseData, $oldData) &&
            $data['user1'] !== $data['user2'];
    }
    
    /**
     * @return string
     */
    protected function getQueueFullName(): string
    {
        return 'queue_db';
    }

}