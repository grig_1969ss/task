<?php


namespace testtask\queue;


use Predis\Client as Predis;

class Data
{
    protected ?Predis $client = null;

    private bool $saved = false;
    private ?int $userId;

    /**
     * Queue constructor.
     * @param array $params
     */
    public function __construct(array $params = [])
    {
        $this->client = Client::getInstance()->redis();
        $this->userId = $params['id'];

        if(isset($params['data'])) {
            $this->client->set(
                $this->getQueueFullName($this->userId),
                json_encode($params['data'])
            );
        }

        return $this;
    }


    /**
     * @return array
     */
    public function getData(): array
    {
        return json_decode($this->client->get($this->getQueueFullName($this->userId)) ?? '', true) ?? [];
    }

    /**
     * Remove current key
     */
    public function remove()
    {
        $this->client->del($this->getQueueFullName($this->userId));
    }

    /**
     * @return bool
     */
    public function saved(): bool
    {
        return $this->saved;
    }

    /**
     * @return string
     */
    protected function getQueueFullName(int $userId): string
    {
        return 'data_' . $this->dataName . '_' . $userId;
    }

}