<?php


namespace testtask\queue;


use testtask\traits\TSingleton;

class Client
{
    use TSingleton;

    public function redis(): \Predis\Client
    {
        return new \Predis\Client([
            'scheme' => 'tcp',
            'host'   => '127.0.0.1',
            'port'   => 6379,
        ]);
    }
}