<?php


namespace testtask\traits;


trait TSingleton
{
    private static $_instance = null;

    private function __clone() {}
    private function __construct() {}

    public static function getInstance(): self
    {
        if(!self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }
}