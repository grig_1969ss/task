<?php


namespace testtask\helpers;


class ArrayHelper
{
    /**
     * @param string $json
     * @return array
     */
    public static function makeArray(string $json): array
    {
        return json_decode($json, true);
    }

    /**
     * @param array $array
     * @return string
     */
    public static function makeJson(array $array = []): string
    {
        return json_encode($array);
    }
}